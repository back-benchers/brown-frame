const validation = require("../validation/auth");
const jwt = require("jsonwebtoken");
const fs = require("fs-extra");
require("dotenv").config();

const books = fs.readJsonSync("data/books.json");
const authors = fs.readJsonSync("data/authors.json");
const users = fs.readJsonSync("data/users.json");

const isAuthenticated = (req, res, next) => {
  var error = validation.token(req);

  if (typeof error.error !== "undefined") {
    res.status(400).send(error.error.details[0].message);
    return;
  }

  const tokenData = req.headers.authorization;
  if (tokenData) {
    const token = tokenData.split(" ");
    req.token = token[1];

    jwt.verify(req.token, process.env.JWT_SECRET, function (err, jwt) {
      if (err) {
        return res.sendStatus(401);
      } else {
        if (!jwt) return res.sendStatus(401);
        const user = users.find((one) => one.email === jwt.user.email);
        if (!user) return res.sendStatus(401);
        req.user = user;
        return next();
      }
    });
  } else {
    return res.sendStatus(401);
  }
};

const isAdmin = (req, res, next) => {
  const user = req.user;
  if (req.user.type === "admin") return next();
  res.sendStatus(401);
};

module.exports = { isAuthenticated, isAdmin };
