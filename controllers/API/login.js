const validation = require("../../validation/login");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const users = require("../../models/users");

const loginUser = (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const user = { email };

  const error = validation.login(req.body);

  if (typeof error.error !== "undefined") {
    res.status(400).send(error.error.details[0].message);
    return;
  }

  userData = users.getUsers().find((one) => one.email === email);
  if (!userData) return res.status(401).send("Email or Password is incorrect");
  if (userData.password !== password)
    return res.status(401).send("Email or Password is incorrect");
  jwt.sign(
    { user },
    process.env.JWT_SECRET,
    { expiresIn: "60m" },
    (err, token) => {
      res.json({ token });
    }
  );
};

module.exports = { loginUser };
