const books = require("../../models/books");

const getBooks = (req, res) => {
  if (req.user.type === "admin") return res.send(books.getBooks());
  const book = books.getBooks().filter((one) => one.isPublished === true);
  if (!book) res.status(404).send("Can not find books");
  res.send(book);
};

const getBook = (req, res) => {
  var book = null;
  if (req.user.type === "admin") {
    book = books.getBooks().find((one) => one.id === parseInt(req.params.id));
  } else {
    book = books
      .getBooks()
      .filter(
        (one) => one.isPublished === true && one.id === parseInt(req.params.id)
      );
  }
  if (!book) res.status(404).send("Can not find book");
  res.send(book);
};

module.exports = { getBooks, getBook };
