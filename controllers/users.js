const users = require("../models/users");

const getUsers = (req, res) => {
  const allUsers = users.getUsers();
  res.render("users/index", { title: "Users", users: allUsers });
};

const newUser = (req, res) => {
  res.render("users/new", { title: "New User" });
};

const editUser = (req, res) => {
  const allUsers = users.getUsers();
  user = allUsers.find((one) => one.id === parseInt(req.params.id));
  res.render("users/edit", { title: "Edit User", user: user });
};

const createUser = (req, res) => {
  users.createUser(req.body);
  return res.redirect("/users");
};

const updateUser = (req, res) => {
  users.updateUser(req.params.id, req.body);
  return res.redirect("/users");
};

const deleteUser = (req, res) => {
  users.deleteUser(req.params.id);
  return res.redirect("/users");
};

module.exports = {
  getUsers,
  newUser,
  editUser,
  createUser,
  updateUser,
  deleteUser,
};
