const Joi = require("joi");

const token = (req) => {
  const token = req.headers.authorization;
  const schema = Joi.object({
    token: Joi.string().required(),
  });
  return schema.validate({ token });
};

module.exports = { token };
