const Joi = require("joi");

const login = (user) => {
  const schema = Joi.object({
    email: Joi.string().min(3).required(),
    password: Joi.string().min(3).required(),
  });
  return schema.validate(user);
};

module.exports = { login };
