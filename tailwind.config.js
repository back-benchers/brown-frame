/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/*.{html,js,css}",
    "./views/*.ejs",
    "./views/partials/*.ejs",
    "./views/users/*.ejs",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
