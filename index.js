const express = require("express");
require("dotenv").config();
const methodOverride = require("method-override");
const app = express();

const web = require("./routes/web.js");
const api = require("./routes/api.js");

app.use(express.urlencoded());
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.json());
app.use(methodOverride("_method"));

app.use("/", web);
app.use("/api", api);

// Environment
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
