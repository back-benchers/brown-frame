const fs = require("fs-extra");

const getBooks = () => {
  const books = fs.readJsonSync("data/books.json");
  return books;
};

module.exports = { getBooks };
