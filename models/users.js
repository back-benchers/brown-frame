const fs = require("fs-extra");

const getUsers = () => {
  const users = fs.readJsonSync("data/users.json");
  return users;
};

const createUser = (data) => {
  var users = [];
  users = fs.readJsonSync("data/users.json");
  const user = {
    id: new Date().getTime(),
    email: data.email,
    type: data.type,
    password: data.password,
  };
  users.push(user);
  const newUser = fs.writeJsonSync("data/users.json", users);

  console.log(users);
  return newUser;
};

const updateUser = (id, data) => {
  console.log(data);
  var users = [];
  users = fs.readJsonSync("data/users.json");
  const user = users.find((one) => one.id === parseInt(id));

  const usersNew = users.map((one) =>
    one.id === parseInt(id) ? { ...one, type: data.type } : one
  );

  const newUser = fs.writeJsonSync("data/users.json", usersNew);

  console.log(newUser);
  return newUser;
};

const deleteUser = (id) => {
  var users = [];
  users = fs.readJsonSync("data/users.json");

  const usersNew = users.filter((one) => {
    return one.id != parseInt(id);
  });

  const newUser = fs.writeJsonSync("data/users.json", usersNew);

  console.log(newUser);
  return newUser;
};

module.exports = { getUsers, createUser, updateUser, deleteUser };
