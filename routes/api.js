const express = require("express");
require("express-group-routes");
const api = express.Router();

const auth = require("./../middleware/auth");

const { getBooks, getBook } = require("../controllers/API/books.js");
const { loginUser } = require("../controllers/API/login.js");

api.group("/v1", (router) => {
  router.post("/login", loginUser);

  router.use("/books", auth.isAuthenticated);

  router.get("/books", getBooks);
  router.get("/books/:id", getBook);
});

api.group("/v2", (router) => {
  router.get("/books", (req, res) => {
    res.send("Not Implemented");
  });
});

module.exports = api;
