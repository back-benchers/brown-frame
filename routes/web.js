const express = require("express");
require("express-group-routes");
const web = express.Router();

const { home } = require("../controllers/home.js");
const {
  getUsers,
  newUser,
  editUser,
  createUser,
  updateUser,
  deleteUser,
} = require("../controllers/users.js");

web.get("/", home);

web.group("/users", (router) => {
  router.get("/", getUsers);
  router.get("/new", newUser);
  router.get("/edit/:id", editUser);

  router.post("/", createUser);
  router.route("/:id").put(updateUser).delete(deleteUser);
});

module.exports = web;
